## depedencies

* **nodejs**: https://nodejs.org/en/download/
* **yarn**: https://yarnpkg.com/en/docs/install/

## install [not required]

```
cd /path_to_project/
yarn
yarn global add live-server
```


## run
```
live-server
```
Then follow the instructions in terminal (open 0.0.0.0:8080)

## demo
```
http://logiq.one:8080/
```